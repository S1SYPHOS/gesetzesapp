export default () => ({
    // Enable fullscreen
    open() {
        const elem = document.documentElement;

        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        }

        /* Safari */
        if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
        }

        /* IE11 */
        if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    },

    // Disable fullscreen
    close() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }

        /* Safari */
        if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }

        /* IE11 */
        if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
})
