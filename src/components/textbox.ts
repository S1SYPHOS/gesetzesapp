export default () => ({
    content: '',

    length() {
        return this.$refs.text.maxLength - this.content.length
    },

    maxLength() {
        return this.$refs.text.maxLength
    },
})
