from os.path import abspath, dirname
from multiprocessing import cpu_count


wsgi_app = 'main:app'
bind = ':1024'
chdir = dirname(abspath(__file__))
workers = cpu_count() * 2 + 1
max_requests = 1024
