import logging


class BaseConfig(object):
    """
    Default config
    """

    # Configure log handler
    # (1) Logfile size limit
    LOG_MAX_BYTES = 1024 * 1024

    # (2) Logfile backup limit
    LOG_BACKUP_COUNT = 5

    # Limit text size to 5000 characters
    MAX_TEXT_LENGTH = 5000


class Development(BaseConfig):
    """
    'Development' config
    """

    # Define logfile
    LOG_FILE = 'debug.log'

    # Define loglevel
    LOG_LEVEL = logging.DEBUG


class Production(BaseConfig):
    """
    'Production' config
    """

    # Define logfile
    LOG_FILE = 'live.log'

    # Define loglevel
    LOG_LEVEL = logging.WARNING


config = {
    'development': Development,
    'production': Production,
}
