from os.path import abspath, dirname, join

from .utils import load_json


def current_version() -> str:
    # Get directory below this one
    path = dirname(dirname(abspath(__file__)))

    try:
        # Load JSON data from `package.json` file
        data = load_json(join(path, 'package.json'))

        # Return current version
        return data['version']

    except:
        pass

    return 'dev'
