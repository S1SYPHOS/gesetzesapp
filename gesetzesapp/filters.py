from os.path import join
from json import dumps
from base64 import b64encode
from hashlib import sha256


def json2string(data: list) -> str:
    """
    Converts JSON data to indented string
    """

    return dumps(data, ensure_ascii = False, indent = 4)


def sri_hash(input_file: str) -> str:
    """
    Generates hash suitable for SRI
    """

    hashed = sha256()

    with open(join('app', 'assets', input_file), 'rb') as file:
        while True:
            data = file.read(65536)

            if not data:
                break

            hashed.update(data)

    hashed = hashed.digest()
    hash_base64 = b64encode(hashed).decode('utf-8')

    return 'sha256-{}'.format(hash_base64)


filters = {
    'json2string': json2string,
    'sri': sri_hash,
}
