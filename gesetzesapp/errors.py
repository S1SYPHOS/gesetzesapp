from flask import redirect, url_for
from werkzeug.exceptions import HTTPException


def handle_error(error: HTTPException):
    return redirect(url_for('api.index'))
