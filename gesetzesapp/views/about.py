from flask import Blueprint, render_template


# Initialize blueprint
about = Blueprint('about', __name__)


@about.route('/')
def index():
    return render_template('pages/about.html')
