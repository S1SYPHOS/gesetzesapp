module.exports = {
    extract: {
        include: [
            'gesetzesapp/templates/**/*.html',
        ],
    },
    safelist: [
        // File field (drag & drop)
        'active inactive',

        // Icons
        'icon-info icon-danger icon-add icon-remove',

        // Modal animation
        'ease-out opacity-0 opacity-100 scale-90 scale-100',
    ],
    darkMode:'class',
    theme: {
        extend: {
            screens: {
                'xs': '480px',
            },
            container: {
                center: true,
            }
        }
    },
    shortcuts: {
        // Generic
        'link': 'font-medium text-blue-500 hover:text-orange-400 hover:text-underline transition-color duration-200',

        // Terminal
        'term-btn': 'w-3 h-3 focus:outline-none shadown-inner rounded-full',

        // Button
        'btn': 'py-2 px-3 font-medium text-white bg-blue-500 hover:bg-orange-400 focus:outline-none focus:bg-orange-400 focus:ring focus:ring-orange-500 focus:ring-opacity-50 rounded-lg cursor-pointer transition-colors duration-200',

        // File field (drag & drop)
        'inactive': 'border-gray-400 hover:border-orange-400',
        'active': 'border-orange-400',

        // Boxes
        'alert': 'py-3 px-4 rounded-lg',
        'info': 'text-blue-700 bg-blue-100',
        'warning': 'text-yellow-700 bg-yellow-100',
        'error': 'text-red-700 bg-red-100',

        // Code preview
        'code': 'px-3 py-4 font-mono text-sm text-blue-gray-200 bg-blue-gray-700 bg-hero-texture-dark-25 overflow-auto',
    },
    variants: {
        extend: {}
    },
    plugins: [
        require('windicss/plugin')(({ addComponents }) => {
            addComponents({
                '[x-cloak]': {
                    'display': 'none',
                },
            })
        }),
        require('windicss/plugin/forms'),
        require('@windicss/plugin-icons'),
        require('@windicss/plugin-heropatterns')({
            patterns: ['graph-paper', 'texture'],
            colors: {
              'default': '#0369a1',
              'dark': '#0f172a',
            },
            opacity: {
                default: '1.0',
                25: '0.25',
            },
        })
    ],
}
