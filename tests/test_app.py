import pytest

from gesetzesapp import create_app


@pytest.fixture()
def app():
    # Create application
    app = create_app()

    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


def test_index(client):
    # Run function
    response = client.get('/')

    # Assert result
    assert response.status_code == 200


def test_about(client):
    # Run function
    response = client.get('/ueber-das-projekt/')

    # Assert result
    assert response.status_code == 200


def test_legal(client):
    # Run function
    response = client.get('/rechtliche-angaben/')

    # Assert result
    assert response.status_code == 200
