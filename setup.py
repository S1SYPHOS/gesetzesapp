import setuptools

# Load README
with open('README.md', 'r', encoding = 'utf8') as file:
    long_description = file.read()

# Define package metadata
setuptools.setup(
    name = 'gesetzesapp',
    version = '1.1.0',
    author = 'Martin Folkers',
    author_email = 'hello@twobrain.io',
    description = 'Webapp linking legal norms',
    long_description = long_description,
    long_description_content_type = 'text/markdown',
    url = 'https://codeberg.org/S1SYPHOS/gesetzesapp',
    license = 'MIT',
    project_urls = {
        'Issues': 'https://codeberg.org/S1SYPHOS/gesetzesapp/issues',
    },
    classifiers = [
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    packages = setuptools.find_packages(),
    package_data = {'gesetzesapp': [
        'assets/favicon.ico',
        'assets/pubkey.asc',
        'assets/css/main.css',
        'assets/js/main.js',
        'templates/*.html',
        'templates/api/*.html',
        'templates/pages/*.html',
        'templates/partials/*.html',
    ]},
    install_requires = [
        'flask',
        'flask-fingerprint',
        'flask-minify',
        'gesetze',
    ],
    python_requires = '>= 3.7'
)
